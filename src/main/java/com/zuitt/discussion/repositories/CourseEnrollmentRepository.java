package com.zuitt.discussion.repositories;

import com.zuitt.discussion.models.CourseEnrollment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseEnrollmentRepository extends CrudRepository<CourseEnrollment, Object> {
}

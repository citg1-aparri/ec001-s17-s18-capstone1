package com.zuitt.discussion.controllers;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    JwtToken jwtToken;


    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        courseService.createCourse(stringToken, course);
        return new ResponseEntity<>("Course created successfully.", HttpStatus.CREATED);
    }

    @RequestMapping(value="/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses(){
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable int courseid, @RequestHeader(value = "Authorization") String stringToken){
        return  courseService.deleteCourse(courseid, stringToken);
    }

    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable int courseid, @RequestHeader(value="Authorization") String stringToken, @RequestBody Course course){
        return courseService.updateCourse(courseid, stringToken, course);
    }

    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourse(@PathVariable int courseid){
        return new ResponseEntity<>(courseService.getCourse(courseid), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/archive/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> archiveCourse(@PathVariable int courseid, @RequestHeader(value="Authorization") String stringToken){
        return courseService.archiveCourse(courseid, stringToken);
    }

    @RequestMapping(value = "/courses/unarchive/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> unarchiveCourse(@PathVariable int courseid, @RequestHeader(value="Authorization") String stringToken){
        return courseService.unarchiveCourse(courseid, stringToken);
    }

    @RequestMapping(value = "/courses/active", method = RequestMethod.GET)
    public ResponseEntity<Object> getActiveCourses(){
        return new ResponseEntity<>(courseService.getActiveCourses(), HttpStatus.OK);
    }

//    @RequestMapping(value="/myPosts", method = RequestMethod.GET)
//    public  ResponseEntity<Object> getMyPosts(@RequestHeader(value = "Authorization") String stringToken){
//        return  new ResponseEntity<>(postService.getMyPosts(stringToken), HttpStatus.OK);
//
//    }
}

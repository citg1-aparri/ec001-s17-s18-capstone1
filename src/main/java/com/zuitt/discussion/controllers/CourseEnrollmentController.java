package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.services.CourseEnrollmentService;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseEnrollmentController {

    @Autowired
    CourseEnrollmentService courseEnrollmentService;
    @RequestMapping(value = "/enroll/{courseid}", method = RequestMethod.POST)
    public ResponseEntity<Object> enroll(@PathVariable int courseid, @RequestHeader(value = "Authorization") String stringToken){
        courseEnrollmentService.enrollCourse(courseid, stringToken);
        return new ResponseEntity<>("Enrolled successfully.", HttpStatus.CREATED);
    }
}

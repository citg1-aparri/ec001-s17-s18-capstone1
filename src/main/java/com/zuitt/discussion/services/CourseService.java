package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    void createCourse(String stringToken, Course course);
    Iterable<Course> getCourses();

    Iterable<Course> getActiveCourses();

    Course getCourse(int id);
    ResponseEntity deleteCourse(int id, String stringToken);

    ResponseEntity updateCourse(int id, String stringToken, Course course);

    ResponseEntity archiveCourse(int id, String stringToken);
    ResponseEntity unarchiveCourse(int id, String stringToken);

//    Iterable<Course> getMyCourse(String stringToken);
}

package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createCourse(String stringToken, Course course) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        courseRepository.save(newCourse);
    }
    public Iterable<Course> getCourses(){
        return courseRepository.findAll();
    }

    public Iterable<Course> getActiveCourses(){
        return courseRepository.findAllByIsActive(true);
    }

    public Course getCourse(int id){
        return courseRepository.findById(id).get();
    }
    public ResponseEntity deleteCourse(int id, String stringToken) {
        courseRepository.deleteById(id);
        return new ResponseEntity<>("Course deleted successfully.", HttpStatus.OK);
//        Course courseForDeleting = courseRepository.findById(id).get();

//        String postAuthorName = postForDeleting.getUser().getUsername();
//        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);
//        if(authenticatedUserName.equals(postAuthorName)){
//            postRepository.deleteById(id);
//            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
//        }else{
//            return new ResponseEntity<>("You are not authorized to delete this post",
//                    HttpStatus.UNAUTHORIZED);
//        }
    }

    public ResponseEntity updateCourse(int id, String stringToken, Course course){
        Course courseForUpdate = courseRepository.findById(id).get();
        courseForUpdate.setName(course.getName());
        courseForUpdate.setDescription(course.getDescription());
        courseForUpdate.setPrice(course.getPrice());
        courseRepository.save(courseForUpdate);
        return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
//        Post postForUpdating = postRepository.findById(id).get();
//        String postAuthorName = postForUpdating.getUser().getUsername();
//        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);
//
//        if(authenticatedUserName.equals(postAuthorName)){
//            postForUpdating.setTitle(post.getTitle());
//            postForUpdating.setContent(post.getContent());
//            postRepository.save(postForUpdating);
//
//            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
//        }else{
//            return new ResponseEntity<>("You are not authorized to edit this post",
//                    HttpStatus.UNAUTHORIZED);
//        }
    }

    public ResponseEntity archiveCourse(int id, String stringToken){
        Course courseToArchive = courseRepository.findById(id).get();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(!authenticatedUser.equals(null)){
            courseToArchive.setActive(false);
            courseRepository.save(courseToArchive);
            return new ResponseEntity<>("Course archived successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to archive this course",
                    HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity unarchiveCourse(int id, String stringToken){
        Course courseToArchive = courseRepository.findById(id).get();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(!authenticatedUser.equals(null)){
            courseToArchive.setActive(true);
            courseRepository.save(courseToArchive);
            return new ResponseEntity<>("Course unarchived successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to unarchive this course",
                    HttpStatus.UNAUTHORIZED);
        }
    }

//    public Iterable<Post> getMyPosts(String stringToken){
//        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
//        return author.getPosts();
//    }
}

package com.zuitt.discussion.services;

import org.springframework.http.ResponseEntity;

public interface CourseEnrollmentService {
    ResponseEntity enrollCourse(int id, String stringToken);
}

package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.CourseEnrollment;
import com.zuitt.discussion.repositories.CourseEnrollmentRepository;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CourseEnrollmentServiceImpl implements CourseEnrollmentService{
    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseEnrollmentRepository courseEnrollmentRepository;
    @Autowired
    JwtToken jwtToken;
    public ResponseEntity enrollCourse(int id, String stringToken) {
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        int userid = userRepository.findByUsername(authenticatedUser).getId();
        if(userid != 0){
            CourseEnrollment enrollment = new CourseEnrollment();
            enrollment.setCourse(courseRepository.findById(id).get());
            enrollment.setUser(userRepository.findById(userid).get());
            enrollment.setDateTimeEnrolled(LocalDateTime.now());
            courseEnrollmentRepository.save(enrollment);
            return new ResponseEntity<>("Enrolled successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to enroll",
                    HttpStatus.UNAUTHORIZED);
        }
    }
}

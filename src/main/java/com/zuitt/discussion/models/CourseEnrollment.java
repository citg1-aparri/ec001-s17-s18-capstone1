package com.zuitt.discussion.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "course_enrollment")
public class CourseEnrollment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name="course_id")
    Course course;
    @ManyToOne
    @JoinColumn(name="user_id")
    User user;
    @Column
    private LocalDateTime dateTimeEnrolled;

    public CourseEnrollment(){}

    public CourseEnrollment(int id, Course course, User user, LocalDateTime dateTimeEnrolled) {
        this.id = id;
        this.course = course;
        this.user = user;
        this.dateTimeEnrolled = dateTimeEnrolled;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getDateTimeEnrolled() {
        return dateTimeEnrolled;
    }

    public void setDateTimeEnrolled(LocalDateTime dateTimeEnrolled) {
        this.dateTimeEnrolled = dateTimeEnrolled;
    }
}

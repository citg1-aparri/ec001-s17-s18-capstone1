package com.zuitt.discussion.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -7774123170207328301L;

    private final String jwttoken;

    public JwtResponse(String jwttoken){
        this.jwttoken  = jwttoken;
    }

    public String getJwttoken() {
        return jwttoken;
    }
}
